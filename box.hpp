//
//  box.hpp
//  breakTime_render_windowFade
//
//  Created by cana on 2017/07/29.
//
//

#ifndef box_hpp
#define box_hpp

#include "ofMain.h"

//#define NUM_SOUNDS 3

class Box
{
private:
    ofVec2f pos;
    float width;
    float height;
    ofColor color;
    float alpha;
    float lineWidth;
    
    ofColor boxcolor[ 5 ];
    int r;
    int colorNumber;//
    
    float angle;
    float spin;
    float firstSpin;
    int slow;
    
    bool boxIn;
    
    bool soundPlay;
    
public:
    
    Box()
    {
        pos.set( 0, 0 );
        width = 10.0;
        height = 10.0;
        
        alpha = 255;
        
        boxcolor[0].set( 100, 175, 195 );
        boxcolor[1].set( 240, 220, 140 );
        boxcolor[2].set( 45, 145, 115 );
        boxcolor[3].set( 150, 110, 165 );
        boxcolor[4].set( 215, 150, 180 );
        
        r = ( ofRandom( 0, 5 ) );
        color.set( boxcolor[ r ] );
        colorNumber = r;//
        
        angle = 0;
        
        spin = 20;
        firstSpin = spin;
        
        lineWidth = 1;
        
        boxIn = false;
    }
    
    ~Box()
    {
        
    }
    
    void setPositions( float _x, float _y ){ pos.set( _x, _y ); }
    void setPosition( ofVec2f _pos ){ pos = _pos; }
    
    void setWidth( float _width ){ width = _width; }
    void setHeight( float _height ){ height = _height; }
    void setColor( ofColor _color ){ color = _color; }
    void setAngle( float _angle ){ angle = _angle; }
    void setBoxIn( bool _boxIn ){ boxIn = _boxIn; }
    float getBoxSize(){ return width * height; }
    float getBoxPosx(){ return pos.x; }
    float getBoxPosy(){ return pos.y; }
    ofColor getColor(int _index){ return color[_index]; }//
    ofColor getColor(){return color;}//
    int getColorNumber(){ return colorNumber; }//
    bool getBoxIn(){ return boxIn; }
    bool getSoundPlay(){ return soundPlay; }
    
    void update()
    {
        //        ofColor nowColor = color;
        
        if( boxIn )
        {
            slow = ofRandom( (int)8, (int)10 );
            angle+=spin;
            
            if( spin > slow ){ spin-=0.1; }
            
            if(( angle > firstSpin * 2 ) && ( angle < firstSpin * 3 )){ soundPlay = true; }
            else
            {
                soundPlay = false;
            }
            
            if( angle >= 360 *5 )
            {
                if( colorNumber < 4 )
                {
                    colorNumber++;
                    
                }
                else
                {
                    colorNumber = 0;
                }
                
                angle = 0;
                boxIn = false;
                spin = firstSpin;
                color.set( boxcolor[colorNumber] );//
            }
        }
    }
    
    void draw()
    {
        ofSetColor( 255, alpha );
        ofSetLineWidth( 2 );
        
        ofPushMatrix();
        ofPushStyle();
        ofTranslate( pos );
        ofRotateX( angle );
        ofSetRectMode( OF_RECTMODE_CENTER );
        ofTranslate( -pos );
        ofSetColor( color, alpha );
        ofFill();
        ofDrawRectangle( pos, width, height );
        ofSetColor( 0 );
        ofNoFill();
        ofDrawRectangle( pos.x, pos.y, width, height );
        ofPopStyle();
        ofPopMatrix();
    }
};


#endif /* box_hpp */
