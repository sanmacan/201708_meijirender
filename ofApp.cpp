#include "ofApp.h"

//--------------------------------------- posX
float box_posX[ BOX_NUM ] = { 40, 132.5, 233.5, 334, 434, 532, 630, 727, 823.5, 918.5, 1013.5, 1110.5,
    22, 89.5, 182.5, 277.5, 375.5, 485.5, 593, 689.5, 785, 881.5, 977.5, 1069, 1137,
    33, 93, 307.5, 418.5, 496.5, 662, 734, 836, 886.5, 1066.5, 1126.5,
    20, 80, 324.5, 447, 703.5, 826, 1079, 1139,
    60, 312.5, 388, 461, 701.5, 775.5, 848.5, 1100,
    20, 89, 266, 335.5, 432.5, 503.5, 655.5, 722.5, 821, 894.5, 1066.5, 1135.5,
    44, 113, 282, 365.5, 469, 675.5, 772, 871, 1045, 1114,
    23.5, 92.5, 265, 326, 422, 501.5, 653.5, 718.5, 812, 887.5, 1070.5, 1139.5,
    43, 112, 293, 382, 474.5, 691, 784, 867.5, 1045.5, 1114.5,
    60, 189, 291, 362, 454, 579.5, 696.5, 769.5, 847.5, 970, 1101,
    16, 75, 166.5, 258, 348, 446, 545, 630.5, 717, 812.5, 908, 1004, 1077, 1131.5,
    16, 75, 166.5, 258, 348, 446, 545, 630.5, 717, 812.5, 908, 1004, 1077, 1131.5,
    45, 117, 192, 289, 362.5, 435, 507.5, 579.5, 680.5, 757.5, 828, 898.5, 969, 1042.5, 1114.5,
    24, 82, 191.5, 298.5, 384, 471, 580, 687, 772.5, 860, 968.5, 1082.5, 1140.5,
    22, 80, 325.5, 444, 714.5, 833, 1079, 1137,
    43, 112, 268.5, 346.5, 438, 501, 661.5, 757, 870, 1044.5, 1113.5,
    45, 114, 269, 338.5, 432, 503.5, 656, 725.5, 820.5, 891.5, 1045.5, 1114.5,
    24, 93, 292.5, 382, 475.5, 691.5, 774.5, 857.5, 1067.5, 1136.5,
    46, 115, 266, 335.5, 432, 503.5, 654.5, 723.5, 820.5, 892, 1044, 1113,
    24, 93, 291.5, 386, 480.5, 680.5, 775, 869, 1067.5, 1136.5,
    46, 115, 268, 340, 436, 505, 655.5, 726, 822.5, 892.5, 1043.5, 1112.5,
    23, 82.5, 191, 304, 391, 472, 579, 686, 771.5, 858.5, 967, 1099.5,
    46, 141.5, 240.5, 337.5, 434, 531.5, 629, 725.5, 822.5, 920.5, 1017.5, 1113,
    43.5, 115.5, 201, 345.5, 491.5, 608.5, 754.5, 872, 959.5, 1088.5
};

//--------------------------------------- posY
float box_posY[ BOX_NUM ] = { 21.5, 60, 94, 128, 162, 196, 230, 264, 298, 327, 414, 436, 470, 504, 538, 565.5, 593, 627, 661, 695, 729, 756, 783 , 374 };

//--------------------------------------- width
float box_width[ BOX_NUM ] = { 80, 105, 97, 104, 96, 100, 96, 98, 95, 95, 95, 99,
    44, 91, 95, 95, 101, 119, 96, 97, 94, 99, 93, 90, 46,
    66, 54, 89, 133, 23, 20, 124, 80, 21, 53, 67,
    40, 80, 123, 122, 103, 142, 78, 42,
    120, 99, 52, 94, 99, 49, 97, 120,
    41, 97, 42, 97, 97, 45, 43, 91, 106, 41, 89, 49,
    88, 50, 74, 93, 114, 83, 110, 88, 46, 92,
    47, 91, 40, 82, 110, 49, 39, 91, 96, 55, 97, 41,
    86, 52, 96, 82, 103, 114, 72, 95, 47, 91,
    120, 138, 66, 76, 108, 143, 91, 55, 101, 144, 118,
    32, 86, 97, 86, 94, 102, 96, 75, 98, 93, 98, 94, 52, 57,
    32, 86, 97, 86, 94, 102, 96, 75, 98, 93, 98, 94, 52, 57,
    90, 54, 96, 98, 49, 96, 49, 95, 107, 47, 94, 47, 94, 53, 91,
    48, 68, 151, 63, 108, 66, 152, 62, 109, 66, 151, 77, 39,
    44, 72, 117, 120, 117, 120, 70, 46,
    86, 52, 47, 109, 74, 52, 55, 136, 90, 45, 93,
    90, 48, 48, 91, 96, 47, 44, 95, 95, 47, 47, 91,
    48, 90, 95, 84, 103, 115, 51, 115, 91, 47,
    92, 46, 42, 97, 96, 47, 41, 97, 97, 46, 44, 94,
    48, 90, 93, 96, 93, 93, 96, 92, 91, 47,
    92, 46, 46, 98, 94, 44, 43, 98, 95, 45, 43, 95,
    46, 73, 144, 82, 92, 70, 144, 70, 101, 73, 144, 121,
    92, 99, 99, 95, 98, 97, 98, 95, 99, 97, 97, 94,
    87, 57, 114, 175, 117, 117, 175, 60, 115, 143
};

//--------------------------------------- height
float box_height[ BOX_NUM ] = { 43, 34, 34, 34, 34, 34, 34, 34, 34, 24, 10, 34, 34, 34, 34, 21, 34, 34, 34, 34, 34, 20, 34, 70 };

//--------------------------------------- window
float window_posX[WINDOW_NUM] = { 138, 526, 915, 138, 526, 915 };
float window_posY[WINDOW_NUM] = { 111, 111, 111, 521, 521, 521 };
//float window_width[WINDOW_NUM] = { 107 };
float window_height[WINDOW_NUM] = { 204, 204, 204, 225, 225, 225 };

//--------------------------------------------------------------
void ofApp::setup()
{
    w = ofGetWidth();
    h = ofGetHeight();
    
    halfWidth = w / 2;
    halfHeight = h / 2;
    
    framerate = 60;
    
    scale = 1.7;
    
//    for( int i =0; i < NUM_POSITIONS; i++ ){ userPos[ i ].set( 1, 1 ); }
    
    ofSetFrameRate( framerate );
    ofSetVerticalSync( true );
    ofSetCurveResolution( 64 );
    
    rereaf.load( "image/rereaf.png" );
    
    ofBackground( 0 );
    
    //------------------------------------------ fade
    startTime = 0.0;
    addTime = 1.5;
    fade = false;
    wColor.set( 0, 0, 0 );//next color
    wLastColor.set( 255, 255, 255 );//previous color
    wCurrentColor.set( 255, 255, 255 );//middle color
    
    //------------------------------------------ osc
    kinectReceiver.setup( KINECT_PORT );
    
    webSender.setup( HOST, PORT );
    findUser = 0;
    sendColor.set( 0, 0, 0 );
    
    systemReciver.setup( SOUND_PORT );
    
    //------------------------------------------ box
    boxVolume = 0.2;
    addVolume = 0;
//    bsound = false;
    bsound = true;
    
    for( int i = 0; i < NUM_SOUNDS; i++ )
    {
        //        string soundName = "sound/sound_0" + ofToString( i ) + ".wav"; //kirakira
        string soundName = "sound/switch_00" + ofToString( i ) + ".wav"; //fuwafuwa
        sound[ i ].load( soundName );
        sound[ i ].setMultiPlay( true );
        sound[ i ].setVolume( boxVolume );
    }
    
    most = 0;
    maxColorNumber = 0;
    lastColor.set( 0, 0, 0 );
    
    for( int i = 0; i < NUM_COLOR; i++ ){ countColorNumber[ i ] = 0; }
    
    for( int i = 0; i < BOX_NUM; i++ )
    {
        Box b;
        if( ( i > 0 )&&( box_posX[ i ] < pre_posX ) ){ numHeight += 1; }
        b.setPositions( box_posX[ i ], box_posY[ numHeight ] );
        b.setWidth( box_width[ i ] );
        b.setHeight( box_height[ numHeight ] );
        
        if(( i == 0 ) || ( box_posX[ i ] < pre_posX ) || ( box_posX[ i + 1 ] < box_posX[ i ] ))
        {
            b.setColor( 255 );
        }
        
        boxes.push_back( b );
        
        pre_posX = box_posX[ i ];
    }
    
    //------------------------------------------ syphon
    syphon.setName( "render" );
    
    //------------------------------------------ gui
    setupGUI();
    gui->loadSettings( "GUI/guiSettings.xml" );
}

//--------------------------------------------------------------
void ofApp::setupGUI()
{
    float xInit  = OFX_UI_GLOBAL_WIDGET_SPACING;
    float length = 255 - xInit;
    int dim      = 16;
    
    gui = new ofxUICanvas( 10, 10, length, ofGetHeight() );
    
    //--------------------------------------- title
    gui->addLabel( "Meiji_Render", OFX_UI_FONT_LARGE );
    
    //--------------------------------------- fps slider
    gui->addSpacer( length - xInit, 2 );
    gui->addWidgetDown( new ofxUILabel( "FPS", OFX_UI_FONT_MEDIUM ) );
    gui->addFPSSlider( "FPS", length - xInit, dim);
    
    gui->addWidgetDown( new ofxUISlider( "Volume", 0, 1, &boxVolume, (length - xInit ), dim ) );
    gui->addWidgetDown( new ofxUILabel( "GUI Save 'S' / GUI Load 'L'", OFX_UI_FONT_MEDIUM ) );
    
    gui->autoSizeToFitWidgets();
    gui->setDrawWidgetPadding( true );
    
    //--------------------------------------- add listener
    ofAddListener( gui->newGUIEvent, this, &ofApp::guiEvent );
    
    gui->loadSettings( "GUI/guiSettings.xml" );
}

//--------------------------------------------------------------
void ofApp::update()
{
    //------------------------------------------ soundVolume
    while ( systemReciver.hasWaitingMessages() )
    {
        ofxOscMessage m;
        systemReciver.getNextMessage( m );
        
        if( m.getAddress() == "/sendVolume" )
        {
            bsound = m.getArgAsBool( 0 );
        }
    }
    
////    ------------------------------------------ osc in meijimura
//    while( kinectReceiver.hasWaitingMessages() )
//    {
//        ofxOscMessage m;
//        kinectReceiver.getNextMessage( m );
//        
//        if( m.getAddress() == "/user/position" )
//        {
//            findSize = m.getArgAsInt( 0 );
//            int findBox  = m.getArgAsInt( 1 );
//            userPos[ findBox ].x = m.getArgAsFloat( 2 ) * scale;
//            userPos[ findBox ].y = m.getArgAsFloat( 3 ) * scale;
//        }
//    }
    
//    ------------------------------------------ osc in 2018NZUEx
    userPositions.erase(userPositions.begin(), userPositions.end());
    
    while( kinectReceiver.hasWaitingMessages() )
    {
        ofxOscMessage m;
        kinectReceiver.getNextMessage( m );
        
        if( m.getAddress() == "/user/position" )
        {
            ofVec2f pos;
            pos.set(m.getArgAsFloat(0), m.getArgAsFloat(1));
            userPositions.push_back(pos);
        }
    }
    
   //------------------------------------------ box
    
    if( bsound )
    {
        if( addVolume >= boxVolume ){ addVolume = boxVolume; }
        else{ addVolume += 0.01;}
        
        for( int i = 0; i < NUM_SOUNDS; i++ ){ sound[i].setVolume( addVolume );}
    }
    else if( !bsound )
    {
        if( addVolume <= 0 ){ addVolume = 0; }
        else{ addVolume -= 0.01; }
        for( int i = 0; i < NUM_SOUNDS; i++ )
        {
            sound[i].setVolume( addVolume );
        }
    }
    
        rSound = ofRandom( (int)NUM_SOUNDS );
    
        for( int i = 0; i < NUM_COLOR; i++ ){ countColorNumber[ i ] = 0; }
    
        for( int i = 0; i < boxes.size(); i++ )
        {
//            if( boxes.at(i).getColor().r == 100 ){ countColorNumber[0]++; }
//            else if( boxes.at(i).getColor().r == 240 ){ countColorNumber[1]++; }
//            else if( boxes.at(i).getColor().r == 45 ){ countColorNumber[2]++; }
//            else if( boxes.at(i).getColor().r == 150 ){ countColorNumber[3]++; }
//            else if( boxes.at(i).getColor().r == 215 ){ countColorNumber[4]++; }
            countColorNumber[boxes.at(i).getColorNumber()]++;
    
            for( int j = 0; j < userPositions.size(); j++ )
            {
                if(( userPositions.at( j ).x > boxes.at( i ).getBoxPosx() - 20 ) && ( userPositions.at( j ).x < boxes.at( i ).getBoxPosx() + 20 )
                   &&( userPositions.at( j ).y > boxes.at( i ).getBoxPosy() - 20 ) && ( userPositions.at( j ).y < boxes.at( i ).getBoxPosy() + 20 ))
                {
                    if( boxes.at( i ).getBoxIn() == false )
                    {
                        if( boxes.at( i ).getColor() == 255 ){ boxes.at( i ).setBoxIn( false ); }
                        else{ boxes.at( i ).setBoxIn( true ); }
                    }
                }
            }
            boxes.at( i ).update();
    
            if( boxes.at( i ).getSoundPlay() )
            {
                sound[ rSound ].play();
            }
        }
    
        most = countColorNumber[ 0 ];
        maxColorNumber = 0;
        for( int i =  0; i < NUM_COLOR; i++ )
        {
            if(( most < countColorNumber[ i ] ) && ( i != 0 ) )
            {
                most = countColorNumber[ i ];
                maxColorNumber = i;
            }
        }
    
        Box b;
        if( lastColor != b.getColor( maxColorNumber ) )
        {
            lastColor = b.getColor( maxColorNumber );
    
            wColor = b.getColor( maxColorNumber );
            startTime = ofGetElapsedTimef();
            fade = true;
    
            sendWEB( lastColor );
        }
    
        if( fade )
        {
            if( ofGetElapsedTimef() < startTime + addTime )
            {
                wCurrentColor.r = ofMap( ofGetElapsedTimef(), startTime, startTime + addTime, wLastColor.r, wColor.r );
                wCurrentColor.g = ofMap( ofGetElapsedTimef(), startTime, startTime + addTime, wLastColor.g, wColor.g );
                wCurrentColor.b = ofMap( ofGetElapsedTimef(), startTime, startTime + addTime, wLastColor.b, wColor.b );
            }
            else
            {
                wLastColor = b.getColor( maxColorNumber );
                fade = false;
            }
        }
    
    
    //------------------------------------------ mouse
//    if( bsound )
//    {
//        if( addVolume >= boxVolume ){ addVolume = boxVolume; }
//        else{ addVolume += 0.005;}
//        
//        for( int i = 0; i < NUM_SOUNDS; i++ )
//        {
//            sound[i].setVolume( addVolume );
//        }
//    }
//    else if( !bsound )
//    {
//        if( addVolume <= 0 ){ addVolume = 0; }
//        else{ addVolume -= 0.005; }
//        for( int i = 0; i < NUM_SOUNDS; i++ )
//        {
//            sound[i].setVolume( addVolume );
//        }
//    }
//    
//    rSound = ofRandom( (int)NUM_SOUNDS );
//    
//    
//    for( int i = 0; i < NUM_COLOR; i++ ){ countColorNumber[ i ] = 0; }
//    
//    for( int i = 0; i < boxes.size(); i++ )
//    {
//        if( boxes.at(i).getColor().r == 100 )
//        {
//            countColorNumber[0]++;
//        }
//        else if( boxes.at(i).getColor().r == 240 )
//        {
//            countColorNumber[1]++;
//        }
//        else if( boxes.at(i).getColor().r == 45 )
//        {
//            countColorNumber[2]++;
//        }
//        else if( boxes.at(i).getColor().r == 150 )
//        {
//            countColorNumber[3]++;
//        }
//        else if( boxes.at(i).getColor().r == 215 )
//        {
//            countColorNumber[4]++;
//        }
//        
//        
//        
//        if(( mouseX > boxes.at( i ).getBoxPosx() - 20 ) && ( mouseX < boxes.at( i ).getBoxPosx() + 20 ) &&
//           ( mouseY > boxes.at( i ).getBoxPosy() - 20 ) && ( mouseY < boxes.at( i ).getBoxPosy() + 20 ) &&
//           ( boxes.at( i ).getBoxIn() == false ))
//        {
//            if( boxes.at( i ).getColor() == 255 )
//            {
//                boxes.at( i ).setBoxIn( false );
//            }
//            else
//            {
//                ofColor nowColor = boxes.at( i ).getColor();
//                boxes.at( i ).setBoxIn( true );
//            }
//        }
//        boxes.at( i ).update();
//        
//        if( boxes.at( i ).getSoundPlay() )
//        {
//            sound[ rSound ].play();
//        }
//    }
//    
//    Box b;
//    most = countColorNumber[ 0 ];//most box color index init
//    maxColorNumber = 0;//now window color index init
//    for( int i =  0; i < NUM_COLOR; i++ )
//    {
//        if(( most < countColorNumber[ i ] ) && ( i != 0 ) )
//        {
//            most = countColorNumber[ i ];//most box color index
//            maxColorNumber = i;//now window color index
//        }
//    }
//    
//    if( lastColor != b.getColor( maxColorNumber ) )
//    {
//        lastColor = b.getColor( maxColorNumber );//now window color
//        
//        wColor = b.getColor( maxColorNumber );
//        startTime = ofGetElapsedTimef();
//        fade = true;
//        
//        sendWEB( lastColor );
//    }
//    
//    if( fade )
//    {
//        if( ofGetElapsedTimef() < startTime + addTime )
//        {
//            wCurrentColor.r = ofMap( ofGetElapsedTimef(), startTime, startTime + addTime, wLastColor.r, wColor.r );
//            wCurrentColor.g = ofMap( ofGetElapsedTimef(), startTime, startTime + addTime, wLastColor.g, wColor.g );
//            wCurrentColor.b = ofMap( ofGetElapsedTimef(), startTime, startTime + addTime, wLastColor.b, wColor.b );
//        }
//        else
//        {
//            wLastColor = b.getColor( maxColorNumber );
//            fade = false;
//        }
//    }
}

//--------------------------------------------------------------
void ofApp::sendWEB( ofColor _inColor )
{
    ofxOscMessage m;
    
    m.setAddress( "/user/boxInUser" );
    m.addIntArg( _inColor.r );
    m.addIntArg( _inColor.g );
    m.addIntArg( _inColor.b );
    
    webSender.sendMessage(m);
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofSetColor( 255 );
    
    //------------------------------------------ window
    Box b;
    for( int i = 0; i < WINDOW_NUM; i++ )
    {
        ofPushStyle();
        ofSetColor( wCurrentColor );
        ofDrawRectangle( window_posX[i], window_posY[i], 107, window_height[i] );
        ofPopStyle();
    }
    
    //------------------------------------------ box
    for( int i = 0; i < boxes.size(); i++ )
    {
        boxes.at( i ).draw();
    }
    
    //------------------------------------------ userPoint
    ofSetRectMode( OF_RECTMODE_CORNER );
    for( int i = 0; i < userPositions.size(); i++ )
    {
        ofFill();
        ofSetColor( 255, 255, 255 );
        ofDrawCircle( userPositions.at(i), 10 );
    }
    
    //------------------------------------------ rereaf
    ofPushStyle();
    ofSetColor( 255, 255 );
    rereaf.draw( 0, 0 );
    ofPopStyle();
    
    //------------------------------------------ syphon
    syphon.publishScreen();
    
    ofDrawBitmapString( ofToString(userPositions.size()), w - 50, h -50 );
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    if( key == 'f' )
    {
        ofToggleFullscreen();
    }
    
    //------------------------------------------
    if( key == 's' )
    {
        gui->saveSettings( "GUI/guiSettings.xml" );
    }
    if( key == 'l' )
    {
        gui->loadSettings( "GUI/guiSettings.xml" );
    }
    
    //    if( key == 'b' )
    //    {
    //        bsound = true;
    //    }
    //
    //    if( key == 'n' )
    //    {
    //        bsound = false;
    //    }
    
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y )
{
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void ofApp::exit()
{
    gui->saveSettings( "GUI/guiSettings.xml" );
    
    delete gui;
    
}

//--------------------------------------------------------------
void ofApp::guiEvent(ofxUIEventArgs &e)
{
    
}
