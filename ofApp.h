#pragma once

#include "ofMain.h"
#include "ofxUI.h"
#include "ofxOsc.h"
#include "box.hpp"
#include "ofxSyphon.h"

#define KINECT_PORT 2513

#define HOST "localhost"
#define PORT 7240

#define SOUND_PORT 5080

#define BOX_NUM 309
#define NUM_COLOR 5
#define WINDOW_NUM 6
//#define NUM_POSITIONS 8
#define NUM_SOUNDS 2

class ofApp : public ofBaseApp
{
public:
    void setup();
    void update();
    void draw();
    void exit();
    
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    
    //------------------------------------------------ basic
    float w, h;
    float halfWidth, halfHeight;
    int framerate;
    float scale;
    ofImage rereaf;
    
    //------------------------------------------------ gui
    ofxUICanvas* gui;
    
    void setupGUI();
    
    void guiEvent( ofxUIEventArgs &e );
    
    //------------------------------------------------ box
    vector< Box > boxes;
    
    float pre_posX = 0;
    int numHeight = 0;
    
    float boxVolume;
    float addVolume;
    
    int countColorNumber[ NUM_COLOR ];
    int most;
    int maxColorNumber;
    ofColor lastColor;
    
    //------------------------------------------------ fade
    ofColor wColor;
    ofColor wLastColor;
    ofColor wCurrentColor;
    float startTime;
    float addTime;
    bool fade;
    
    //----------------------------------------------- osc
    ofxOscReceiver kinectReceiver;
//    ofVec2f userPos[ NUM_POSITIONS ];
    vector<ofVec2f>userPositions;
    
    ofxOscSender webSender;
    int findUser;
    ofColor sendColor;
    void sendWEB( ofColor _inColor );
    
    ofxOscReceiver systemReciver;
    bool bsound;
    
    
    //----------------------------------------------- syphon
    ofxSyphonServer syphon;
    
    //----------------------------------------------- sounds
    ofSoundPlayer sound[NUM_SOUNDS];
    int rSound;
//    float volume;
    

};
